# Capes OS

Capes-OS est un système d'exploitation utilisé lors des épreuves orales du concours du [Capes / Cafep NSI](https://capes-nsi.org).

Ce système est basé sur la distribution Linux [NixOS](https://nixos.org/).

Une version préliminaire de ce système est mise à disposition des personnes se préparant au concours afin qu'elles puissent se familiariser avec l'environnement.

La documentation [est en cours de rédaction](https://capes-cafep-nsi.gitlab.io/OS/).
