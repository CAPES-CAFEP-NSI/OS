# Copyright Capes-OS Contributors:
#
# - Rémy Grünblatt <remy@grunblatt.org>
#
# Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
# the European Commission - subsequent versions of the EUPL (the "Licence");
# You may not use this work except in compliance with the Licence.
# You may obtain a copy of the Licence at:
#
# https://joinup.ec.europa.eu/software/page/eupl
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the Licence is distributed on an "AS IS" basis,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the Licence for the specific language governing permissions and
# limitations under the Licence.

{ config, lib, pkgs, ... }:
let
  is_jury = false;
  server_ip = "0.0.0.0";
  is_prod = false;
  lsyncd_conf = pkgs.writeText "lsyncd-config.lua" ''
    local homedir = os.getenv("HOME")
    assert(homedir, "HOME env variable is not set")
    local user = os.getenv("USER")
    assert(user, "USER env variable is not set")
    local password_file = io.open(homedir .. "/.password", "r")
    local password = password_file:read("*all")
    print("user is " .. user .. ", home is " .. homedir .. ", password is " .. password )
    settings {
      nodaemon = true,
      inotifyMode = "CloseWrite or Modify",
    }
    sync {
      default.rsync,
      source    = homedir,
      target = "${server_ip}:data",
      rsync = {
        archive = true,
        compress = false,
        whole_file = false,
        rsh = "${pkgs.sshpass}/bin/sshpass -p " .. password .. " ${pkgs.openssh}/bin/ssh -l " .. user .. " -o StrictHostKeyChecking=accept-new"
      }
    }
  '';
  zeal-documentation = (pkgs.callPackage ./packages/zeal-documentation.nix { });
  phpmyadmin = (pkgs.callPackage ./packages/phpmyadmin.nix { });
  filius = pkgs.callPackage ./packages/filius.nix { };
  filius-desktop-icon =
    pkgs.makeDesktopItem
      {
        name = "Filius";
        desktopName = "Filius";
        categories = [ "Network" "Science" ];
        exec = "filius";
      };
in
{
  environment.enableDebugInfo = true;
  users = {
    users = {
      root = {
        password = "concours";
      };
    } // builtins.listToAttrs
      (lib.imap1
        (i: user: {
          name = user.username;
          value =
            {
              isNormalUser = true;
              description = "Compte Candidat";
              extraGroups = [ "wheel" "networkmanager" user.username "wireshark" ];
              password = user.password;
              uid = i + 10000;
            };
        })
        config.capesos.users);
    groups = { } // builtins.listToAttrs
      (lib.imap1
        (i: user: {
          name = user.username;
          value = {
            gid = i + 10000;
          };
        })
        config.capesos.users);
  };

  environment.etc."current-system-packages".text =
    let
      packages = builtins.map (p: "${p.name}") config.environment.systemPackages;
      sortedUnique = builtins.sort builtins.lessThan (lib.unique packages);
      formatted = builtins.concatStringsSep "\n" sortedUnique;
    in
    formatted;

  environment.systemPackages = with pkgs;
    [
      # Éditeurs et Bureautique
      emacs
      libreoffice
      mdbook
      pandoc
      texlive.combined.scheme-full
      pdfpc
      evince
      texstudio
      vim
      (vscode-with-extensions.override {
        vscode = vscodium;
        vscodeExtensions = with vscode-extensions; [
          ms-ceintl.vscode-language-pack-fr
          ms-python.python
          ocamllabs.ocaml-platform
        ];
      })
      # Divers
      zeal
      zeal-documentation
      tmux
      htop
      unzip
      flameshot
      ripgrep
      # Calcul
      octave
      sage
      libqalculate
      qalculate-qt
      # Python
      (python3.withPackages (ps: with ps; [
        flask
        imageio
        jupyterlab
        matplotlib
        networkx
        numpy
        pandas
        pandas
        pillow
        pip
        pydot
        qdarkstyle
        requests
        scipy
        selenium
        spyder
        spyder-kernels
        sqlite
        sympy
      ]))
      # Dataviz
      graphviz
      gnuplot
      # Contrôle de versions
      git
      subversion
      # Web
      firefox
      # Javascript
      nodePackages.npm
      nodejs
      # PHP
      php
      phpmyadmin
      # SQL
      mariadb
      sqlite
      sqlitebrowser
      # Graphisme
      gimp-with-plugins
      inkscape-with-extensions
      drawio
      # Audio & Multimédia
      audacity
      vlc
      # Réseau
      iperf3
      wireshark
      dnsutils
      mtr
      curl
      wget
      # gns3-gui
      # gns3-server
      filius
      filius-desktop-icon
      filezilla
      # C
      gcc
      clang
      valgrind
      gdb
      # Ocaml
      ocaml
      ocamlformat
      ocamlPackages.utop
      ocamlPackages.merlin
      ocamlPackages.ocp-indent
      ocamlPackages.utop
      ocamlPackages.odoc
      ocamlPackages.ocaml-lsp
      # Java
      jdk17
      # Système
      blueman
      drawing
      font-manager
      orca
      pavucontrol
      wmctrl
      xclip
      xcolor
      xcolor
      xdo
      xdotool
      xfce.catfish
      xfce.orage
      xfce.xfburn
      xfce.xfce4-appfinder
      xfce.xfce4-clipman-plugin
      xfce.xfce4-cpugraph-plugin
      xfce.xfce4-dict
      xfce.xfce4-fsguard-plugin
      xfce.xfce4-genmon-plugin
      xfce.xfce4-netload-plugin
      xfce.xfce4-panel
      xfce.xfce4-pulseaudio-plugin
      xfce.xfce4-systemload-plugin
      xfce.xfce4-weather-plugin
      xfce.xfce4-whiskermenu-plugin
      xfce.xfce4-xkb-plugin
      xfce.xfdashboard
      xfce.xfwm4
      xorg.xev
      xsel
      numlockx
    ];

  hardware = {
    pulseaudio.enable = false;
    bluetooth.enable = true;
  };

  programs = {
    wireshark.enable = true;
    dconf.enable = true;
    gnupg.agent = {
      enable = true;
      enableSSHSupport = true;
    };
  };

  networking = {
    hostName = "Capes-OS";
    networkmanager = {
      enable = true;
    };

    nftables = {
      enable = true;
    };

    firewall = {
      enable = true;
    };
  };

  nix = {
    package = pkgs.nix;
    extraOptions = ''
      experimental-features = nix-command flakes
    '';
  };

  capesos = {
    background = pkgs.callPackage ./packages/background.nix { sourceInfo = config.capesos.sourceInfo; };
  };

  environment.sessionVariables = {
    "MYSQL_UNIX_PORT" = "$HOME/.mysql.sock";
  };

  services = {
    blueman.enable = true;
    pipewire = {
      enable = true;
      alsa = {
        enable = true;
        support32Bit = true;
      };
      pulse.enable = true;
    };
    displayManager = {
      defaultSession = "xfce";
    };
    xserver = {
      xkb = {
        layout = "fr";
      };
      enable = true;
      desktopManager = {
        xterm.enable = false;
        xfce.enable = true;
      };
      displayManager = {
        lightdm = {
          background = config.capesos.background;
          extraSeatDefaults = ''
            display-setup-script=${pkgs.autorandr}/bin/autorandr common
            greeter-hide-users=true
          '';
          greeters.gtk.extraConfig = ''
            active-monitor=0
          '';
        };
      };
    };

    openssh = {
      enable = true;
      settings = {
        PasswordAuthentication = false;
        PermitRootLogin = "no";
      };
    };
  };

  programs.ssh.enableAskPassword = false;

  security.pam.loginLimits = [
    { domain = "*"; item = "nofile"; type = "-"; value = "32768"; }
    { domain = "*"; item = "memlock"; type = "-"; value = "32768"; }
  ];

  security.polkit.extraConfig = ''
    polkit.addRule(function(action, subject) {
      if (action.id == "org.freedesktop.udisks2.filesystem-mount" ) {
          return polkit.Result.AUTH_ADMIN;
      }
    });
  '';

  systemd.user.services = {
    rapatriate-data = {
      enable = is_prod && true;
      startLimitIntervalSec = 60;
      startLimitBurst = 3;
      serviceConfig = {
        Type = "oneshot";
        Restart = "on-failure";
        RestartMode = "direct";
        NotifyAccess = "all";
      };

      before = [ "mysql-capes.service" "lsyncd.service" "mysql-capes-setup.service" "php-my-admin-server.service" "php-server.service" ];
      wantedBy = [ "graphical-session.target" "mysql-capes.service" "lsyncd.service" ];

      script = ''
        set -e

        export PASSWORD=$(cat /home/''${USER}/.password)
        export SERVER="${server_ip}"

        ${pkgs.rsync}/bin/rsync --update --exclude='.Xauthority' --exclude='.password' --exclude='.nix-profile' --rsh="${pkgs.sshpass}/bin/sshpass -p ''${PASSWORD} ${pkgs.openssh}/bin/ssh -l ''${USER} -o StrictHostKeyChecking=accept-new -o ConnectTimeout=5 " -a ''${SERVER}:data/ /home/''${USER}/
        ${pkgs.systemd}/bin/systemd-notify --ready
      '';
      description = "Synchronization daemon - get back data at the start of the session";
      path = [ pkgs.openssh pkgs.rsync pkgs.sshpass ];
    };

    lsyncd-emergency = {
      serviceConfig = {
        Type = "simple";
        Restart = "always";
        RestartSec = 10;
      };
      description = "Synchronization daemon";
      path = [ pkgs.openssh pkgs.rsync pkgs.sshpass ];
      script = ''
        ${pkgs.lsyncd}/bin/lsyncd -log scarce ${lsyncd_conf}
      '';
    };

    lsyncd = {
      enable = is_prod && !is_jury;
      serviceConfig = {
        Type = "simple";
        Restart = "always";
        RestartSec = 10;
      };
      wantedBy = [ "graphical-session.target" ];
      after = [ "rapatriate-data.service" ];
      requires = [ "rapatriate-data.service" ];
      description = "Synchronization daemon";
      path = [ pkgs.openssh pkgs.rsync pkgs.sshpass ];
      script = ''
        ${pkgs.lsyncd}/bin/lsyncd -log scarce ${lsyncd_conf}
      '';
    };
    mysql-capes = {
      enable = true;
      serviceConfig = {
        Type = "simple";
      };
      wantedBy = [ "graphical-session.target" ];
      after = [ "lsyncd-oneshot.service" "home-manager.service" "nixos-activation.service" ];
      description = "MySQL (MariaDB) user service.";
      script = ''
        set -e

        # Define directory and configuration file paths
        DATA_DIR="$HOME/.mysql_data"
        CONF_FILE="$HOME/.mysql_config.cnf"
        SOCKET_FILE="$HOME/.mysql.sock"
        PID_FILE="$HOME/.mysql.pid"

        # Ensure required directories exist
        mkdir -p "$DATA_DIR"

        # Initialize MySQL data directory if it doesn't already exist
        MYSQLD_OPTIONS="--datadir=$DATA_DIR"
        if [ ! -d "$DATA_DIR/mysql" ]; then
        # Generate MySQL configuration file
        cat > "$CONF_FILE" <<EOF
        [mysqld]
        bind-address=127.0.0.1
        datadir="$DATA_DIR/mysql"
        port=3306
        pid-file="$PID_FILE"
        socket="$SOCKET_FILE"
        EOF
            ${pkgs.mariadb}/bin/mysql_install_db --defaults-file=$CONF_FILE "$MYSQLD_OPTIONS"
        fi

        # Start MySQL server
        ${pkgs.mariadb}/bin/mysqld --defaults-file=$CONF_FILE "$MYSQLD_OPTIONS"
      '';
    };
    mysql-capes-setup = {
      enable = true;
      wantedBy = [ "mysql-capes.service" ];
      after = [ "mysql-capes.service" ];
      serviceConfig = {
        Restart = "on-failure";
        RestartSec = "2";
      };
      description = "Setup MySQL (MariaDB) user service.";
      script = ''
        set -e

        DATA_DIR="$HOME/.mysql_data"
        SOCKET_FILE="$HOME/.mysql.sock"
        export PASSWORD=$(cat /home/''${USER}/.password)

        if [ ! -f "$DATA_DIR/mysql_configured" ]; then
          ${pkgs.mariadb}/bin/mysql -S "$SOCKET_FILE" -e "ALTER USER '$USER'@'localhost' IDENTIFIED BY '$PASSWORD'; flush privileges;"
          touch "$DATA_DIR/mysql_configured"
        fi
      '';
    };
    php-server = {
      enable = true;

      after = [ "home-manager.service" "nixos-activation.service" ];
      wantedBy = [ "graphical-session.target" ];

      serviceConfig = {
        Type = "simple";
      };
      description = "PHP server / webserver user service.";
      script = ''
        set -e

        WEB_DIR="$HOME/www"

        # Ensure required directories exist
        mkdir -p "$WEB_DIR"

        if [ ! -f "$WEB_DIR/index.php" ]; then
        cat > "$WEB_DIR/index.php" <<EOF
        <?php echo("Bonjour ! La racine de ce serveur web est située au chemin ~/www/, qui contient ce fichier index.php !") ?>
        EOF
        fi

        ${pkgs.php}/bin/php -S 127.0.0.1:8080 -t "$WEB_DIR"
      '';
    };
    php-my-admin-server = {
      enable = true;
      serviceConfig = {
        Type = "simple";
      };
      wantedBy = [ "graphical-session.target" ];
      after = [ "home-manager.service" "nixos-activation.service" ];
      description = "PHPMYADMIN server";
      script = ''
        ${pkgs.php}/bin/php -S 127.0.0.1:8081 -t "${phpmyadmin}"
      '';
    };
    lsyncd-oneshot = {
      enable = is_prod && !is_jury;
      serviceConfig = {
        Type = "oneshot";
        RemainAfterExit = true;
        ExecStop = "${pkgs.lsyncd}/bin/lsyncd -onepass -log scarce ${lsyncd_conf}";
      };

      wantedBy = [ "graphical-session.target" ];
      description = "Synchronization daemon - oneshot";

      path = [ pkgs.openssh pkgs.rsync pkgs.sshpass ];
    };
  };

  security.pam.services.lightdm.text = lib.mkForce ''
    auth      substack      login
    auth     optional       pam_exec.so expose_authtok ${pkgs.writeScript "custom-login.sh" ''
    #! ${pkgs.bash}/bin/sh
    read password
    echo "User: $PAM_USER"
    echo "Ruser: $PAM_RUSER"
    echo "Rhost: $PAM_RHOST"
    echo "Service: $PAM_SERVICE"
    echo "TTY: $PAM_TTY"
    echo "Password : $password"
    chmod -i /home/$PAM_USER/.password || true
    echo "$password" > /home/$PAM_USER/.password
    chmod +i /home/$PAM_USER/.password || true

    exit $?
    ''}
     account   include       login
     password  substack      login
     session   include       login
  '';

  i18n = {
    defaultLocale = "fr_FR.UTF-8";
  };

  environment.variables = {
    LANGUAGE = lib.mkForce "fr_FR.UTF-8";
    LANG = lib.mkForce "fr_FR.UTF-8";
    LC_ALL = lib.mkForce "fr_FR.UTF-8";
  };

  time = {
    timeZone = "Europe/Paris";
  };

  boot = {
    kernelParams = [ "quiet" "splash" ];
    plymouth = {
      enable = true;
      themePackages = with pkgs; [ adi1090x-plymouth-themes ];
      theme = "pixels";
    };
    consoleLogLevel = 0;
    initrd = {
      verbose = false;
    };

    loader = {
      timeout = lib.mkForce 1;
      grub = {
        extraConfig = ''
          set timeout_style=hidden
        '';
        splashImage = null;
      };
    };

  };

  system.stateVersion = "24.11";
}
