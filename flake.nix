# Copyright (c) 2019 lassulus and the nixos-generators contributors
#
# Copyright Capes-OS Contributors:
#
# - Rémy Grünblatt <remy@grunblatt.org>
#
# Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
# the European Commission - subsequent versions of the EUPL (the "Licence");
# You may not use this work except in compliance with the Licence.
# You may obtain a copy of the Licence at:
#
# https://joinup.ec.europa.eu/software/page/eupl
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the Licence is distributed on an "AS IS" basis,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the Licence for the specific language governing permissions and
# limitations under the Licence.

{
  inputs = {
    nixpkgs = {
      url = "github:NixOS/nixpkgs/release-24.11";
    };
    nixos-generators = {
      url = "github:nix-community/nixos-generators";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    home-manager = {
      url = "github:pasqui23/home-manager/nixos-late-start";
      # url = "github:rgrunbla/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    ressources = {
      url = "gitlab:CAPES-CAFEP-NSI/Ressources-Officielles";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };
  outputs = { self, nixpkgs, nixos-generators, home-manager, ressources, ... } @ inputs:
    let
      system = "x86_64-linux";
      sourceInfo = inputs.self.sourceInfo;
      default_users = (builtins.fromJSON (builtins.readFile ./users.json)).users;
    in
    {
      nixosModules = {
        Capes-OS = { config, lib, ... }: {
          imports = [
            (
              { config, lib, pkgs, ... }:
                with lib;
                {
                  options.capesos =
                    {
                      sourceInfo = mkOption {
                        type = types.attrsOf types.str;
                        default = sourceInfo;
                      };
                      background = mkOption {
                        type = types.path;
                        default = null;
                      };
                      ressources = mkOption {
                        type = types.path;
                        default = ressources.packages.x86_64-linux.default;
                      };
                      users = mkOption {
                        type = lib.types.listOf (lib.types.attrsOf lib.types.str);
                        default = default_users;
                      };
                      release-tag = mkOption
                        {
                          type = types.str;
                          default = "capesOS-${builtins.substring 0 8 (sourceInfo.lastModifiedDate or sourceInfo.lastModified)}-${sourceInfo.shortRev or "dirty"}";
                        };
                    };
                }
            )
            nixos-generators.nixosModules.all-formats
            ./configuration.nix
            ./additional-configuration.nix
            home-manager.nixosModules.home-manager
            {
              home-manager.useGlobalPkgs = true;
              home-manager.useUserPackages = true;
            }
            ./home.nix
            {
              nix.settings = {
                experimental-features = [ "nix-command" "flakes" ];
                auto-optimise-store = true;
              };
              nixpkgs.config.allowUnfree = true;
            }
          ];

          nixpkgs.hostPlatform = "x86_64-linux";

          # customize an existing format
          formatConfigs =
            {
              # Quick test on NixOS
              vm = {
                virtualisation = {
                  memorySize = 4096;
                  cores = 4;
                  diskSize = 5000;
                };
              };

              custom-raw-bios = { config, lib, pkgs, modulesPath, ... }: {
                fileSystems."/" = {
                  device = "/dev/disk/by-label/nixos";
                  fsType = "ext4";
                };

                boot = {
                  growPartition = true;
                  kernelParams = [ "console=ttyS0" ];
                  loader = {
                    grub = {
                      device = lib.mkDefault "/dev/vda";
                    };
                    timeout = lib.mkDefault 0;
                  };
                  initrd.availableKernelModules = [ "uas" ];
                };

                system.build.raw = import "${toString modulesPath}/../lib/make-disk-image.nix" {
                  inherit lib config pkgs;
                  name = config.capesos.release-tag;
                  diskSize = "auto";
                  bootSize = "512M";
                  format = "raw";
                  postVM = ''
                    cd $out
                    ${pkgs.zstd}/bin/zstd --rm -T0 $diskImage -o nixos.bios.img.zst
                  '';
                };

                formatAttr = "raw";
                fileExtension = lib.mkForce ".bios.img.zst";
              };

              # Custom compressed raw efi format
              custom-raw-efi = { config, lib, pkgs, modulesPath, ... }: {
                fileSystems = {
                  "/" = {
                    device = "/dev/disk/by-label/nixos";
                    fsType = "ext4";
                  };
                  "/boot" = {
                    device = "/dev/disk/by-label/ESP";
                    fsType = "vfat";
                  };
                };

                boot = {
                  growPartition = true;
                  kernelParams = [ "console=ttyS0" ];
                  loader = {
                    grub = {
                      device = "nodev";
                      efiSupport = true;
                      efiInstallAsRemovable = true;
                    };
                    timeout = lib.mkDefault 0;
                  };
                  initrd.availableKernelModules = [ "uas" ];
                };

                system.build.raw = import "${toString modulesPath}/../lib/make-disk-image.nix" {
                  inherit lib config pkgs;
                  name = config.capesos.release-tag;
                  partitionTableType = "efi";
                  diskSize = 55000;
                  bootSize = "512M";
                  format = "raw";
                  postVM = ''
                    cd $out
                    ${pkgs.zstd}/bin/zstd --rm -T0 $diskImage -o nixos.uefi.img.zst
                  '';
                };


                formatAttr = "raw";
                fileExtension = ".uefi.img.zst";
              };

              # Live system
              iso = {
                isoImage = {
                  isoBaseName = config.capesos.release-tag;
                };
              };

              # Ova images
              virtualbox = { config, pkgs, ... }: {
                virtualbox = {
                  baseImageSize = 60 * 1024;
                  memorySize = 4096;
                  vmDerivationName = config.capesos.release-tag;
                  vmName = config.capesos.release-tag;
                  vmFileName = "${config.capesos.release-tag}.ova";
                  params = {
                    cpus = 4;
                  };
                };
                virtualisation.virtualbox.guest.enable = true;
              };

            };
        };
        Capes-OS-Server = { config, lib, ... }: {
          imports = [
            (
              { config, lib, pkgs, ... }:
                with lib;
                {
                  options.capesos =
                    {
                      sourceInfo = mkOption {
                        type = types.attrsOf types.str;
                        default = sourceInfo;
                      };
                      background = mkOption {
                        type = types.path;
                        default = null;
                      };
                      users = mkOption {
                        type = lib.types.listOf (lib.types.attrsOf lib.types.str);
                        default = default_users;
                      };
                      release-tag = mkOption
                        {
                          type = types.str;
                          default = "capesOS-${builtins.substring 0 8 (sourceInfo.lastModifiedDate or sourceInfo.lastModified)}-${sourceInfo.shortRev or "dirty"}";
                        };
                    };
                }
            )
            nixos-generators.nixosModules.all-formats
            ./server-configuration.nix
            {
              nix.settings = {
                experimental-features = [ "nix-command" "flakes" ];
                auto-optimise-store = true;
              };
              nixpkgs.config.allowUnfree = true;
            }
          ];

          nixpkgs.hostPlatform = "x86_64-linux";

          # customize an existing format
          formatConfigs =
            {
              # Quick test on NixOS
              vm = {
                virtualisation = {
                  memorySize = 4096;
                  cores = 4;
                  diskSize = 5000;
                };
              };

              # Ova images
              virtualbox = { config, pkgs, ... }: {
                virtualbox = {
                  baseImageSize = 60 * 1024;
                  memorySize = 4096;
                  vmDerivationName = config.capesos.release-tag;
                  vmName = config.capesos.release-tag;
                  vmFileName = "${config.capesos.release-tag}.ova";
                  params = {
                    cpus = 4;
                  };
                };
                virtualisation.virtualbox.guest.enable = true;
              };

            };
        };
      };

      # the evaluated machine
      nixosConfigurations = {
        Capes-OS = nixpkgs.lib.nixosSystem
          {
            inherit system;
            modules = [ self.nixosModules.Capes-OS ];
          };

        Capes-OS-Server = nixpkgs.lib.nixosSystem
          {
            inherit system;
            modules = [ self.nixosModules.Capes-OS-Server ];
          };
      };

      packages.${system} =
        with import
          nixpkgs
          { inherit system; };
        let
          ova = self.nixosConfigurations.Capes-OS.config.formats.virtualbox;
          iso = self.nixosConfigurations.Capes-OS.config.formats.iso;
          bios = self.nixosConfigurations.Capes-OS.config.formats.custom-raw-bios;
          efi = self.nixosConfigurations.Capes-OS.config.formats.custom-raw-efi;
          vm = self.nixosConfigurations.Capes-OS.config.formats.vm;
        in
        {
          inherit ova iso bios efi vm;
          ova-server = self.nixosConfigurations.Capes-OS-Server.config.formats.virtualbox;
          default = stdenv.mkDerivation
            {
              name = "all-the-medias";
              dontUnpack = true;
              buildPhase = ''
                mkdir -p $out
                ln -s ${ova} $out/
                ln -s ${iso} $out/
                ln -s ${bios} $out/
                ln -s ${efi} $out/
              '';
            };
        };
      devShell.${system} = with import nixpkgs { inherit system; }; mkShell {
        buildInputs = [
          pre-commit
        ];
      };
    };
}
