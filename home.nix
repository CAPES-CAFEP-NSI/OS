# Copyright Capes-OS Contributors:
#
# - Rémy Grünblatt <remy@grunblatt.org>
#
# Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
# the European Commission - subsequent versions of the EUPL (the "Licence");
# You may not use this work except in compliance with the Licence.
# You may obtain a copy of the Licence at:
#
# https://joinup.ec.europa.eu/software/page/eupl
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the Licence is distributed on an "AS IS" basis,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the Licence for the specific language governing permissions and
# limitations under the Licence.

{ config, pkgs, lib, osConfig, ... }:

{
  home-manager.useUserService = true;
  home-manager.users = { }
    // builtins.listToAttrs
    (lib.imap1
      (i: user: {
        name = user.username;
        value = { lib, osConfig, ... }: {
          xsession.initExtra = "${pkgs.autorandr}/bin/autorandr common";
          home = {
            username = user.username;
            enableNixpkgsReleaseCheck = false;
            homeDirectory = "/home/${user.username}";
            stateVersion = "24.11";

            activation = {
              installZealDocsets =
                let
                  zeal-documentation = (pkgs.callPackage ./packages/zeal-documentation.nix { });
                in
                lib.hm.dag.entryAfter [ "writeBoundary" ] ''
                  mkdir -p ~/.local/share/Zeal/Zeal/docsets;
                  ln -sf ${zeal-documentation}/docsets ~/.local/share/Zeal/Zeal/docsets
                '';
              createDesktopIcon =
                let
                  documentation = osConfig.capesos.ressources;
                in
                lib.hm.dag.entryAfter [ "writeBoundary" ] ''
                  mkdir -p ~/Bureau/
                  cat > ~/Bureau/Ressources.desktop <<EOF
                  [Desktop Entry]
                  Version=1.0
                  Type=Application
                  Name=Ressources
                  Comment=
                  Icon=emblem-new
                  Exec=firefox file://${documentation}/index.html
                  EOF
                  chmod +x ~/Bureau/Ressources.desktop
                  ${pkgs.glib}/bin/gio set -t string ~/Bureau/Ressources.desktop metadata::xfce-exe-checksum "$(${pkgs.coreutils}/bin/sha256sum ~/Bureau/Ressources.desktop | ${pkgs.gawk}/bin/awk '{print $1}')"
                '';

            };
          };
          xfconf.settings = {
            xfce4-desktop =
              let
                workspaces = map (i: "workspace${toString i}") (lib.range 0 3);
                monitors = [
                  "monitorHDMI-1"
                  "monitorHDMI-2"
                  "monitoreDPI-1"
                  "monitorVirtual-1"
                ];
                makeProperty = monitor: workspace:
                  "backdrop/screen0/${monitor}/${workspace}/last-image";
                properties = lib.cartesianProduct {
                  monitor = monitors;
                  workspace = workspaces;
                };
              in
              lib.listToAttrs
                (map
                  (prop: {
                    name = makeProperty prop.monitor prop.workspace;
                    value = "${osConfig.capesos.background}";
                  })
                  properties
                );

            xfce4-screensaver = {
              "saver/idle-activation/delay" = 60;
            };
            xfce4-power-manager = {
              "xfce4-power-manager/dpms-on-ac-off" = 0;
              "xfce4-power-manager/dpms-on-ac-sleep" = 0;
              "xfce4-power-manager/blank-on-ac" = 0;
              "xfce4-power-manager/brightness-switch" = 0;
              "xfce4-power-manager/dpms-enabled" = false;
            };
            xfwm4 = {
              "general/placement" = "0";
              "general/wrap_windows" = "false";
            };
          };

          xdg.mimeApps = {
            enable = true;
            associations.added = {
              "application/pdf" = [ "org.gnome.Evince.desktop" ];
            };
            defaultApplications = {
              "application/pdf" = [ "org.gnome.Evince.desktop" ];
            };
          };

          services = {
            flameshot = {
              enable = true;
              settings = {
                General = {
                  showStartupLaunchMessage = false;
                };
              };
            };
          };

          gtk = {
            enable = true;
            iconTheme = {
              name = "elementary-Xfce-dark";
              package = pkgs.elementary-xfce-icon-theme;
            };
            theme = {
              name = "yaru";
              package = pkgs.yaru-theme;
            };
            gtk3.extraConfig = {
              gtk-application-prefer-dark-theme = 0;
            };
            gtk4.extraConfig = {
              gtk-application-prefer-dark-theme = 0;
            };
          };

          systemd.user.targets.tray = {
            Unit = {
              Description = "Home Manager System Tray"; # See https://github.com/nix-community/home-manager/issues/2064
              Requires = [ "graphical-session-pre.target" ];
            };
          };

          services.gpg-agent.enable = true;
          programs = {
            gpg = {
              enable = true;
            };
            firefox = {
              enable = true;
              package = pkgs.wrapFirefox pkgs.firefox-bin-unwrapped {
                extraPolicies = {
                  CaptivePortal = false;
                  DisableFirefoxStudies = true;
                  DisablePocket = true;
                  DisableTelemetry = true;
                  DisableFirefoxAccounts = false;
                  NoDefaultBookmarks = false;
                  OfferToSaveLogins = false;
                  OfferToSaveLoginsDefault = false;
                  PasswordManagerEnabled = false;
                  FirefoxHome = {
                    Search = true;
                    Pocket = false;
                    Snippets = false;
                    TopSites = false;
                    Highlights = false;
                  };
                  Homepage = {
                    StartPage = "homepage";
                    URL = "https://capes-nsi.org/";
                  };
                  OverrideFirstRunPage = "https://capes-nsi.org/";
                  UserMessaging = {
                    ExtensionRecommendations = false;
                    SkipOnboarding = true;
                  };
                  ExtensionSettings = { };
                  Extensions = {
                    "Install" = [
                      (pkgs.fetchurl {
                        url = "https://addons.mozilla.org/firefox/downloads/latest/ublock-origin/latest.xpi";
                        hash = "sha256-ip4CqoOMMC+xTitbyIpgNtNjWKrdb5UWihRa8gGO8aM=";
                      })
                    ];
                  };
                };
              };
              profiles = {
                "Profil par défaut" = {
                  settings = {
                    "browser.translations.autoTranslate" = false;
                    "browser.translations.automaticallyPopup" = false;
                  };
                  bookmarks = [
                    {
                      name = "CAPES NSI Bookmarks"; # Bookmark Folder
                      toolbar = true;
                      bookmarks =
                        [
                          { name = "Documentation du système"; url = "https://capes-cafep-nsi.gitlab.io/OS/"; }
                          { name = "Capes NSI"; url = "https://capes-nsi.org/"; }
                          { name = "Serveur Web"; url = "http://localhost:8080/"; }
                          { name = "PhpMyAdmin"; url = "http://localhost:8081/"; }
                          {
                            name = "Ressources Officielles";
                            url = "file://${osConfig.capesos.ressources}/index.html";
                          }
                        ];
                    }
                  ];
                  search = {
                    default = "zoTop";
                    engines = {
                      "Amazon.com".metaData.hidden = true;
                      "Wikipedia (en)".metaData.hidden = true;
                      "Bing".metaData.hidden = true;
                      "DuckDuckGo".metaData.hidden = true;
                      "zoTop" = {
                        urls = [{
                          template = "https://zotop.zaclys.com/";
                          params = [
                            { name = "q"; value = "{searchTerms}"; }
                          ];
                        }];
                      };
                    };
                    force = true;
                  };
                };
              };
            };
            home-manager = {
              enable = true;
            };
          };
        };
      })
      config.capesos.users);
}
