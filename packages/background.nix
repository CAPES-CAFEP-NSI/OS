# Copyright Capes-OS Contributors:
#
# - Rémy Grünblatt <remy@grunblatt.org>
#
# Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
# the European Commission - subsequent versions of the EUPL (the "Licence");
# You may not use this work except in compliance with the Licence.
# You may obtain a copy of the Licence at:
#
# https://joinup.ec.europa.eu/software/page/eupl
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the Licence is distributed on an "AS IS" basis,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the Licence for the specific language governing permissions and
# limitations under the Licence.

{ stdenv, lib, imagemagick, makeFontsConf, liberation_ttf, sourceInfo }:

stdenv.mkDerivation rec {
  pname = "customized-background";
  version = "0.0.1";
  src = ../medias/background.jpg;
  dontUnpack = true;
  buildPhase = ''
    export XDG_CACHE_HOME="$(mktemp -d)";
    export FONTCONFIG_FILE=${makeFontsConf { fontDirectories = [ liberation_ttf ]; }};
    ${imagemagick}/bin/convert -font '@${liberation_ttf}/share/fonts/truetype/LiberationSans-Regular.ttf' $src -pointsize 24 -fill white -annotate +1280+1055 "Capes NSI OS − ${builtins.substring 0 8 (sourceInfo.lastModifiedDate or sourceInfo.lastModified)}-${sourceInfo.shortRev or "dirty"}" -direction right-to-left $out
  '';
}
