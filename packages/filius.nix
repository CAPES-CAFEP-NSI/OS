# Copyright Capes-OS Contributors:
#
# - Rémy Grünblatt <remy@grunblatt.org>
#
# Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
# the European Commission - subsequent versions of the EUPL (the "Licence");
# You may not use this work except in compliance with the Licence.
# You may obtain a copy of the Licence at:
#
# https://joinup.ec.europa.eu/software/page/eupl
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the Licence is distributed on an "AS IS" basis,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the Licence for the specific language governing permissions and
# limitations under the Licence.

{ stdenv, lib, fetchzip, jdk }:

stdenv.mkDerivation rec {
  pname = "filius";
  version = "2.5.1";

  src = fetchzip {
    url = "https://www.lernsoftware-filius.de/downloads/Setup/${pname}-${version}.zip";
    hash = "sha256-UrtmL0jHf86vXXCtRJoaXAyElBvUUF1jGpBcO0GV6Lg=";
    stripRoot = false;
  };

  installPhase = ''
    mkdir -p $out/bin
    cp -a $src/. $out/

    cat > $out/bin/filius << EOF
    #!/bin/sh

    # See https://gitlab.com/filius1/filius/-/issues/94
    exec ${jdk}/bin/java -Dswing.defaultlaf=javax.swing.plaf.metal.MetalLookAndFeel -Dswing.crossplatformlaf=javax.swing.plaf.metal.MetalLookAndFeel -Dswing.aatext=true -Dawt.useSystemAAFontSettings=on \$@ -jar $out/filius.jar
    EOF

    chmod +x $out/bin/filius
  '';
}
