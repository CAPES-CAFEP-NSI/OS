# Copyright Capes-OS Contributors:
#
# - Rémy Grünblatt <remy@grunblatt.org>
#
# Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
# the European Commission - subsequent versions of the EUPL (the "Licence");
# You may not use this work except in compliance with the Licence.
# You may obtain a copy of the Licence at:
#
# https://joinup.ec.europa.eu/software/page/eupl
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the Licence is distributed on an "AS IS" basis,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the Licence for the specific language governing permissions and
# limitations under the Licence.

{ stdenv, fetchurl, lib, zeal }:

stdenv.mkDerivation rec {
  pname = "ressources-officielles";
  version = "0.0.1";

  sourceRoot = ".";
  srcs = map
    (lang: fetchurl {
      url = "http://sanfrancisco.kapeli.com/feeds/${lang.name}.tgz";
      hash = lang.hash;
    }) [

    {
      name = "Bash";
      hash = "sha256:17m1mpvpniiqd8gjrhzjwnqw0ik302rj3l5hqrfby31xplwlc6yv";
    }

    {
      name = "C";
      hash = "sha256:0bla19dz7ab4izdkz5xkx5vhwjni9758mrprzkfzmc132nn2aa4f";
    }

    {
      name = "C++";
      hash = "sha256:1pwqwyggs677qn39jabjxdp1gkzw505frgizjza8ipi2gi0d949q";
    }

    {
      name = "CSS";
      hash = "sha256:0vb5r845sjhk52s2cbaf9ijshpklif3rib12hrf7650bc3r65dqr";
    }

    {
      name = "Flask";
      hash = "sha256:1h49bszg4k4aimrf75iqd5r64cz6r3k4fs82wcnmz37hpwjdppsr";
    }

    {
      name = "HTML";
      hash = "sha256:0q8p4b09sl1yvpy1sas66bgkph5s3f0qalab209sq61iwgam47kh";
    }

    {
      name = "Java";
      hash = "sha256:0f38f7q79md276v2qyyyx8vd28vrzwvh19jk3a4a23cn34q8zb1b";
    }

    {
      name = "LaTeX";
      hash = "sha256:08ybgfh0anm388sbcyb35cas28sqiljpac4agpgfv9b42ix2cxdp";
    }

    {
      name = "Markdown";
      hash = "sha256:1cybjbrdcbakq5qqdy71xn420vrv9h6550b9m257nmp53237m122";
    }

    {
      name = "Matplotlib";
      hash = "sha256:1zcqadk1jvinskzcmb66968ggbjb9vvmjnsvrs1hpiradg2p4fhm";
    }

    {
      name = "MySQL";
      hash = "sha256:13bfb79k5n0b9a58ngpd5i1rb174qy02d90zw1v0cslqadc5cfal";
    }

    {
      name = "NodeJS";
      hash = "sha256:0i7lvlk28vklfd2jbi2lis9k5bnwl9ksfmzrjkn4qmswbf6vc3x9";
    }

    {
      name = "NumPy";
      hash = "sha256:01hg0l648zxxxqwbwkc0w5qxw2nclxlkm0jhbzblspzqxils9lny";
    }

    {
      name = "OCaml";
      hash = "sha256:05ahi9rcw5zmcw610q3k6skbni146y69r1yh06fwnksrrx6xd7mb";
    }

    {
      name = "PHP";
      hash = "sha256:01h07fp4xpi8szcxn3n71qxqqnsprmajl0fmvyb61mwy33364hyk";
    }

    {
      name = "Python_3";
      hash = "sha256:0wry93zs4gsj9nxcw2lk8q3mn9d6xhxn44hw70pa3hv7zzrxglis";
    }

    {
      name = "SciPy";
      hash = "sha256:12syr6hf017m6s7hp0vg6zr4jbicxnk14fm2ym0ya7xjq61i2v7p";
    }

    {
      name = "SQLite";
      hash = "sha256:1iifbd2x5pnaqlhn011lv7lh6wf27p3jswwrwjx02nbpcnx6m495";
    }

    {
      name = "Vim";
      hash = "sha256:16p3zcqwx9n3ishnpwp19wzzkm2fq08nnkns4sgdpl5jwylmzhvb";
    }
  ];

  buildInputs = [
    zeal
  ];

  installPhase = ''
    mkdir -p $out/docsets
    cp -r *.docset $out/docsets/
    ls -alh $out/docsets/
  '';
}
