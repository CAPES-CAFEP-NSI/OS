# Copyright Capes-OS Contributors:
#
# - Rémy Grünblatt <remy@grunblatt.org>
#
# Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
# the European Commission - subsequent versions of the EUPL (the "Licence");
# You may not use this work except in compliance with the Licence.
# You may obtain a copy of the Licence at:
#
# https://joinup.ec.europa.eu/software/page/eupl
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the Licence is distributed on an "AS IS" basis,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the Licence for the specific language governing permissions and
# limitations under the Licence.

{ stdenv, fetchurl, lib, zeal }:

stdenv.mkDerivation rec {
  pname = "zeal-documentation";
  version = "0.0.1";

  sourceRoot = ".";
  srcs = map
    (lang: fetchurl {
      url = "http://sanfrancisco.kapeli.com/feeds/${lang.name}.tgz";
      hash = lang.hash;
    }) [

    {
      name = "Bash";
      hash = "sha256:17m1mpvpniiqd8gjrhzjwnqw0ik302rj3l5hqrfby31xplwlc6yv";
    }

    {
      name = "C";
      hash = "sha256:0bla19dz7ab4izdkz5xkx5vhwjni9758mrprzkfzmc132nn2aa4f";
    }

    {
      name = "C++";
      hash = "sha256:1pwqwyggs677qn39jabjxdp1gkzw505frgizjza8ipi2gi0d949q";
    }

    {
      name = "CSS";
      hash = "sha256:1387ziznhwmz6ypkg6qd113gjy9b5skrcbjps0f05y15019s0xx4";
    }

    {
      name = "Flask";
      hash = "sha256:1h49bszg4k4aimrf75iqd5r64cz6r3k4fs82wcnmz37hpwjdppsr";
    }

    {
      name = "HTML";
      hash = "sha256:0qh284wd5xg7rdbymm8nry0dbqyskwvqic7kr661ksh57qkk6bwd";
    }

    {
      name = "Java";
      hash = "sha256:0f38f7q79md276v2qyyyx8vd28vrzwvh19jk3a4a23cn34q8zb1b";
    }

    {
      name = "LaTeX";
      hash = "sha256:0iqm7ixqk9lj4jrs0hhczx71hflrdxz3vliisggxwv4370fb96na";
    }

    {
      name = "Markdown";
      hash = "sha256:1cybjbrdcbakq5qqdy71xn420vrv9h6550b9m257nmp53237m122";
    }

    {
      name = "Matplotlib";
      hash = "sha256:15vq8n2byz66fcm2kayp04lqjpfgfg2yl4l0rbpwxp2f0nr0hg20";
    }

    {
      name = "MySQL";
      hash = "sha256:13bfb79k5n0b9a58ngpd5i1rb174qy02d90zw1v0cslqadc5cfal";
    }

    {
      name = "NodeJS";
      hash = "sha256:0bikjvgr55vk8bkixzfpynp3n9xnmz6ys5iq6w20qxg7linwnri3";
    }

    {
      name = "NumPy";
      hash = "sha256:02rc4g5yp49f7h5s0x9x5b4fhr8piwqaavi54dwwjwklmxp8jarl";
    }

    {
      name = "OCaml";
      hash = "sha256:05ahi9rcw5zmcw610q3k6skbni146y69r1yh06fwnksrrx6xd7mb";
    }

    {
      name = "PHP";
      hash = "sha256:1lyzjyc4xq7cc43yx80gpmq73q27ghxlpfyqd6pyni0g1r4jx20z";
    }

    {
      name = "Python_3";
      hash = "sha256:0mq3l27jlw0ix2ax5d93dsyz7qm0nn1xfghnha5al304a77p6763";
    }

    {
      name = "SciPy";
      hash = "sha256:0m6rj3nc73s46hwyra9fyrb0i5pm54h4baqj49c9jzi94ykbkd2z";
    }

    {
      name = "SQLite";
      hash = "sha256:0gjbznnha1n9x424jaz5ghhimrx0ksgj6py5ypih948drz0j1p12";
    }

    {
      name = "Vim";
      hash = "sha256:16p3zcqwx9n3ishnpwp19wzzkm2fq08nnkns4sgdpl5jwylmzhvb";
    }
  ];

  buildInputs = [
    zeal
  ];

  installPhase = ''
    mkdir -p $out/docsets
    cp -r *.docset $out/docsets/
    ls -alh $out/docsets/
  '';
}
