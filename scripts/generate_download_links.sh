#!/usr/bin/env bash

nix build --max-jobs 0 $(dirname $0)/../.#
for file in $(find $(dirname $0)/../result/ -type f);
do
	$(dirname $0)/filesender.py -r remy@grunblatt.org ${file}
done
