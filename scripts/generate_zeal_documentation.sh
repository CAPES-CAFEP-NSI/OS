#!/usr/bin/env nix-shell
#! nix-shell -i bash -p nix nixpkgs-fmt

# Copyright Capes-OS Contributors:
#
# - Rémy Grünblatt <remy@grunblatt.org>
#
# Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
# the European Commission - subsequent versions of the EUPL (the "Licence");
# You may not use this work except in compliance with the Licence.
# You may obtain a copy of the Licence at:
#
# https://joinup.ec.europa.eu/software/page/eupl
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the Licence is distributed on an "AS IS" basis,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the Licence for the specific language governing permissions and
# limitations under the Licence.

for language in {"Bash","C","C++","CSS","Flask","HTML","Java","LaTeX","Markdown","Matplotlib","MySQL","NodeJS","NumPy","OCaml","PHP","Python_3","SciPy","SQLite","Vim"}
do
        url="http://sanfrancisco.kapeli.com/feeds/${language}.tgz";
	hash=$(nix-prefetch-url "${url}" 2>/dev/null);
	echo "
{
  name = \"${language}\";
  hash = \"sha256:${hash}\";
}"
done > zeal-documentation.hashes

cat zeal-documentation.nix.head zeal-documentation.hashes zeal-documentation.nix.tail > ../packages/zeal-documentation.nix
nixpkgs-fmt ../packages/zeal-documentation.nix
