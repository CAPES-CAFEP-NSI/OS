CREATE USER IF NOT EXISTS 'candidat'@'localhost' IDENTIFIED BY 'concours';
GRANT ALL ON *.* to 'candidat'@'localhost';
REVOKE SUPER ON *.* from 'candidat'@'localhost';
FLUSH PRIVILEGES;
