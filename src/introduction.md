# Introduction

Capes-OS est un système d'exploitation utilisé lors des épreuves orales du concours du [Capes / Cafep NSI](https://capes-nsi.org), à partir de l'année 2023 / 2024.

Ce système est basé sur la distribution Linux [NixOS](https://nixos.org/).

Une version préliminaire de ce système est mise à disposition des personnes se préparant au concours afin qu'elles puissent se familiariser avec l'environnement.

## Captures d'écran

[ ![Capture d'écran de Capes-OS avec filieus et le menu des applications bureautiques ouvert](./medias/filius-bureautique.png)](./medias/filius-bureautique.png)

[ ![Capture d'écran de Capes-OS où on peut voir la documentation hors-ligne Zeal et VSCodium ouvert avec le code de la documentation recopié](./medias/zeal-vscodium.png)](./medias/zeal-vscodium.png)

