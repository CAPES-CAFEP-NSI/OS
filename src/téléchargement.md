# Téléchargement

Les liens de téléchargement pour les différentes versions du système sont les suivants :

- [Image de machine virtuelle](https://remy.grunblatt.org/capesOS-latest.ova)
- [Image ```.iso``` de système live](https://remy.grunblatt.org/capesOS-latest.iso)

